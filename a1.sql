INSERT INTO users (email, password, datetime_created ) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");

INSERT INTO users (email, password, datetime_created ) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");

INSERT INTO users (email, password, datetime_created ) VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00");

INSERT INTO users (email, password, datetime_created ) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");

INSERT INTO users (email, password, datetime_created ) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");


INSERT INTO post (title, content, datetime_posted, author_id ) VALUES ("first Code", "Hello World!", "2021-01-02 01:00:00", 1);

INSERT INTO post (title, content, datetime_posted, author_id ) VALUES ("Second Code", "Hello Hello Earth", "2021-01-02 02:00:00", 1);

INSERT INTO post (title, content, datetime_posted, author_id ) VALUES ("Third Code", "Welcome to Mars!", "2021-01-02 03:00:00", 2);

INSERT INTO post (title, content, datetime_posted, author_id ) VALUES ("Bye Bye solar System", "Hello World!", "2021-01-02 04:00:00", 4);

SELECT * FROM post WHERE author_id = 1;
SELECT email, datetime_created FROM users;

UPDATE post SET content = "Hello to the people of the Earth" WHERE content = "Hello Hello Earth";

DELETE FROM users WHERE email = "johndoe@gmail.com";
